var html5_tests = (function() {

    var supports_geolocation = function () {
        return 'geolocation' in navigator;
    }

    supports_canvas_text = function () {
        if (!supports_canvas()) { return false; }
        var dummy_canvas = document.createElement('canvas');
        var context = dummy_canvas.getContext('2d');
        return typeof context.fillText == 'function';
    }

    supports_canvas = function() {
        return !!document.createElement('canvas').getContext;
    }

    supports_video = function() {
        return !!document.createElement('video').canPlayType;
    }

    supports_h264_baseline_video = function() {
        if (!supports_video()) { return false; }
        var v = document.createElement("video");
        return v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
    }

    supports_ogg_theora_video = function() {
        if (!supports_video()) { return false; }
        var v = document.createElement("video");
        return v.canPlayType('video/ogg; codecs="theora, vorbis"');
    }

    supports_webm_video = function() {
        if (!supports_video()) { return false; }
        var v = document.createElement("video");
        return v.canPlayType('video/webm; codecs="vp8, vorbis"');
    }

    supports_local_storage = function() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch(e){
            return false;
        }
    }

    return {
        supports_geolocation: supports_geolocation,
        supports_canvas_text: supports_canvas_text,
        supports_canvas: supports_canvas,
        supports_video: supports_video,
        supports_h264_baseline_video: supports_h264_baseline_video,
        supports_ogg_theora_video: supports_ogg_theora_video,
        supports_webm_video: supports_webm_video,
        supports_local_storage: supports_local_storage
    }

}());
